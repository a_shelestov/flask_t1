#!/usr/bin/env python2
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openpyxl import Workbook, load_workbook

issue_scm = ('id', 'action', 'user', 'date', 'team', 'desc')
issue_scm_sql = ('id', 'title', 'action', 'action_date', 'user_id',
                 'description', 'severity', 'ccb_priority')
user_scm_sql = ('id', 'password_hash', 'corporate_id', 'first_name',
                'last_name', 'team', 'is_admin')
xls_scm = ('B', 'C', 'D', 'E', 'F', 'G')
from datetime import datetime


class IntIterator:
    def __init__(self, initial_val=0): self.i = initial_val

    def v(self):
        self.i += 1
        return self.i

    def s(self): return str(self.v())

    def c(self): return self.i

    def cs(self): return str(self.c())


def xls2dict(filename):
    issues = []
    wb = load_workbook(filename)
    ws = wb.get_sheet_by_name(u'PRs')

    for r in xrange(10, 958):
        issue = {}
        for i in xrange(0, 6):
            issue[issue_scm[i]] = ws[xls_scm[i]+str(r)].value
            if issue_scm[i] == 'user'\
                    and issue[issue_scm[i]] == 'Anton Minialo (uidr2577 )':
                issue[issue_scm[i]] = 'Anton Minialo (uidr2577)'
            if issue_scm[i] == 'team'\
                and issue[issue_scm[i]] == 'Navi':
                issue[issue_scm[i]] = 'Navigation'
        issues.append(issue)
    return issues


def select_unique(dict, key):
    unique = []
    for r in dict:
        if r[key] not in unique:
            unique.append(r[key])
            print(r[key])
    return unique


def validate_act(action):
    if action == 'Analysed':
        return 'Analyzed'
    return action


def dict_reform(dict):
    issues = {}
    users = {}
    u_id = IntIterator()
    i_id = IntIterator()
    for r in dict:

        if r['user'] not in users:
            u_list = r['user'].split(' ')
            u_list[2] = u_list[2].replace('(', '').replace(')', '')
            users[r['user']] = {'first_name': u_list[0],
                                'last_name': u_list[1],
                                'user_id': u_list[2],
                                'team': r['team'],
                                'id': u_id.s()}
        issues[i_id.s()] = {'action': validate_act(r['action']),
                            'action_date': r['date'],
                            'title': r['id'],
                            'description': r['desc'].replace('\'', '\'\'') if r['desc'] else '',
                            'severity': 'Any',
                            'ccb_priority': 'Any',
                            'user_id': users[r['user']]['id'],
                            'comment': r['user']}
    users_new = {}
    for u in users:
        users_new[users[u]['id']] = users[u]
        users_new[users[u]['id']]['comment'] = u

    return {'issues': issues, 'users': users_new}


def dict2sql(dict):
    insert_issuse_prefix = 'INSERT INTO `issues` (`id`, `title`, `action`, ' \
                           '`action_date`, `user_id`, `description`, ' \
                           '`severity`, `ccb_priority`) \nVALUES'
    insert_user_prefix = 'INSERT INTO `users` (`id`, ' \
                         '`password_hash`, `corporate_id`, `first_name`, ' \
                         '`last_name`, `team`, `is_admin`)\nVALUES'
    print(insert_issuse_prefix)
    for i in xrange(1, len(dict['issues']) + 1):
        iid = str(i)
        i = dict['issues'][iid]
        print('(' + iid + ', \'' + i['title'] + '\', \'' + i['action'] +
              '\', \'' + i['action_date'].strftime("%Y-%m-%d") + '\', ' +
              i['user_id'] + ', \'' + i['description'] + '\', \'' +
              i['severity'] + '\', \'' + i['ccb_priority'] +
              '\'),  -- user = ' + i['comment'])
    print('\n' + insert_user_prefix)
    for u in xrange(1, len(dict['users']) + 1):
        uid = str(u)
        u = dict['users'][uid]
        print('(' + u['id'] + ', \'5f4dcc3b5aa765d61d8327deb882cf99\', \'' +
              u['user_id'] + '\', \'' + u['first_name'] + '\', \'' +
              u['last_name'] + '\', \'' + u['team'] + '\', 0),  -- ' +
              u['comment'])

d = xls2dict(sys.argv[1])
d2 = dict_reform(d)
dict2sql(d2)
