__author__ = 'Andrew Shelestov'

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os
import hashlib
import copy
from datetime import datetime
from openpyxl import Workbook
from flask import abort, flash, redirect, url_for, make_response

import flask_acc_config as f_conf
import schemas as shm


user_table_schema = [['id', 0], ['password_hash', 1], ['corporate_id', 2],
                     ['first_name', 3], ['last_name', 4],
                     ['team', 5], ['is_admin', 6]]


def password_hash(password_str):
    m = hashlib.md5()
    m.update(password_str)
    return m.hexdigest()


def user_logged_in(session):
    if session is None:
        return False
    if 'logged_in' not in session:
        return False
    return session['logged_in']


def filter_text(text):
    # print repr('incomming text:' + text) # repr is for representing specials
    repls = (('\r', ''), ('\n', ''), ('   ', ' '))
    for repl in repls:
        text = text.replace(repl[0], repl[1])
    text = text.strip()
    # print repr('outcomming text:' + text)
    return text


def pr_err(func_name='', error='', route=''):
    print('ERROR in ' + func_name + ':' + error)
    flash('ERROR: ' + error)
    if route != '':
        redirect(url_for(route))


def query_wrap(mysql, query_text):
    cur = mysql.connection.cursor()
    cur.execute(query_text)
    mysql.connection.commit()


def user_is_admin(mysql, user_id):
    _users = get_users(mysql)
    if user_id in _users:
        if _users[user_id]['is_admin']:
            return True
    return False


def get_users(active_mysql_object, col_name=None, col_val=None):
    query = 'SELECT * FROM users;'
    str_type = basestring,
    if sys.version_info[0] == 3:
        str_type = str,

    if isinstance(col_val, int):
        col_val = str(col_val)
    elif isinstance(col_val, str_type):
        col_val = "'" + col_val + "'"
    if isinstance(col_name, str_type) and isinstance(col_val, str_type):
        if col_name in ('id', 'corporate_id', 'team'):
            query = 'SELECT * FROM users WHERE ' + \
                    col_name + '=' + col_val + ' ;'
    cur = active_mysql_object.connection.cursor()
    cur.execute(query)

    users = {}
    j = 1L
    for row in cur.fetchall():
        user = {}
        for name, i in user_table_schema:
            user[name] = row[i]
        users[j] = user
        j = (j + 1L)
    return users


def date_valid(date_str):
    try:
        datetime.strptime(date_str, "%Y-%m-%d")
    except ValueError:
        return False
    return True


def get_issues(active_mysql_object, issue_id=0, dates=None, team_name=None,
               user_id=None, limit=0):
    cur = active_mysql_object.connection.cursor()
    if team_name and team_name not in ('Navigation', 'Speech'):
        team_name = None
    query = 'SELECT * FROM issues WHERE id LIKE ' + \
            (str(issue_id) if issue_id > 0 else '\'%\'')
    if team_name:
        query += ' and user_id IN (SELECT id FROM users WHERE team LIKE \'' +\
                 team_name + '\')'
    if dates and dates['from'] and dates['to']:
        query += ' and action_date BETWEEN \'' \
                 + dates['from'] + '\' and \'' + dates['to'] + '\''
        # TODO Consider adding 'order by action_date desc'
    query += ' ORDER BY action_date DESC '
    if limit and int(limit) > 0:
        query += ' LIMIT ' + str(limit)
    query += ';'
    # print(query)
    cur.execute(query)
    issues = [dict(id=row[0], title=row[1], action=row[2], action_date=row[3],
                   user_id=row[4], description=row[5], severity=row[6],
                   ccb_priority=row[7]) for row in cur.fetchall()]
    if not issues:
        return None
    if issue_id != 0:
        return issues[0]
    return issues


def dump_issues2xlsx(active_mysql_object, out_filename):
    issues = get_issues(active_mysql_object)
    users = get_users(active_mysql_object)

    wb = Workbook()
    ws = wb.active

    ws['A1'] = 'ID'
    ws['B1'] = 'Case Id'
    ws['C1'] = 'Action'
    ws['D1'] = 'Action By'
    ws['E1'] = 'On Date'
    ws['F1'] = 'Speech/Navi'
    ws['G1'] = 'Case Name'
    ws['H1'] = 'CCB Priority'
    ws['I1'] = 'Severity'

    for issue in issues:
        user = users[issue['user_id']]
        row = [ str(issue['id']),
                str(issue['title']),
                str(issue['action']),
                str(user['first_name']) + ' ' + str(user['last_name']) + \
                    ' (' + user['corporate_id'] + ')',
                str(issue['action_date']),
                str(user['team']),
                str(issue['description']),
                str(issue['ccb_priority']),
                str(issue['severity']) ]
        ws.append(row)
    wb.save(out_filename)


def dump_all2mysql(username, password, database, filename):
    # TODO Consider rewriting in Python-way
    os.system('mysqldump -u ' + username + ' -p' + password + ' ' + database +
              ' > ' + filename)


def rqf_valid(session, _func, rqf):
    if not user_logged_in(session):
        abort(401)
        return False

    for field in ['title', 'description', 'action_date']:
        if field not in rqf:
            pr_err(_func, 'No ' + field + ' given!')
            return False

    if not date_valid(rqf['action_date']):
        pr_err(_func, 'Given date is invalid!')
        return False
    return True


def sql_strvld(string):
    # print("incomming = " + str(string))
    rv = str(string).replace('"', '\\"')
    # print("retbval = " + rv)
    return rv


def add_issue(mysql, session, rqf):  # rqf for Request form
    __FUNCTION__ = sys._getframe().f_code.co_name + '()'

    if not rqf_valid(session, __FUNCTION__, rqf):
        return

    query_wrap(mysql,
                'INSERT INTO issues'
                '(title, action, action_date, user_id,'
                ' description, severity, ccb_priority)'
                'values ('
                '"' + sql_strvld(rqf['title']) + '",'
                '"' + sql_strvld(rqf['action']) + '", '
                '"' + sql_strvld(rqf['action_date']) + '", '
                '"' + sql_strvld(session['user_id']) + '", '
                '"' + filter_text(sql_strvld(rqf['description'])) + '", '
                '"' + sql_strvld(rqf['severity']) + '", '
                '"' + sql_strvld(rqf['ccb_priority']) + '");'
                )
    return


def rights_sufficient(mysql, session, issue):
    if user_is_admin(mysql, session['user_id']) or \
                     session['user_id'] == issue['user_id']:
        return True
    return False


def edit_issue(mysql, session, rqf, issue):
    __FUNCTION__ = sys._getframe().f_code.co_name + '()'

    if not rqf_valid(session, __FUNCTION__, rqf):
        return False

    if rights_sufficient(mysql, session, issue):
        query_wrap(mysql, 'UPDATE issues SET '
                          'title="' + sql_strvld(rqf['title']) + '", '
                          'action="' + sql_strvld(rqf['action']) + '", '
                          'action_date="' + sql_strvld(rqf['action_date']) +
                          '", description ="' +
                          filter_text(sql_strvld(rqf['description'])) + '", '
                          'severity="' + sql_strvld(rqf['severity']) + '", '
                          'ccb_priority="' + sql_strvld(rqf['ccb_priority']) +
                          '" WHERE id=' + sql_strvld(issue['id']) + ';')
        return True
    return False


def del_issue(mysql, session, issue):
    if rights_sufficient(mysql, session, issue):
        query_wrap(mysql, 'DELETE FROM issues WHERE id = ' +
                   str(issue['id']) + ' LIMIT 1;')


def password_update(mysql, user_id, given_old, new):
    cur = mysql.connection.cursor()
    if user_id is None or user_id <= 0:
        return False

    query = 'SELECT password_hash FROM users WHERE id='\
            + str(user_id) + ' LIMIT 1;'
    cur.execute(query)
    real_old_hash = cur.fetchall()[0][0]
    if password_hash(given_old) == real_old_hash:
        query_wrap(mysql, 'UPDATE users SET password_hash="' +
                   sql_strvld(password_hash(new)) +
                   '" WHERE id=' + str(user_id) + ';')
        return True
    return False


def calculate_stats(mysql, team_name, dates=None):
    # TODO Test with non consequest users IDs!

    if team_name not in ('Navigation', 'Speech'):
        team_name = None
    sql_users = get_users(mysql, col_name='team', col_val=team_name)
    sql_issues = get_issues(mysql, dates=dates, team_name=team_name)

    users = {}
    total = shm.gen_user()
    for action in shm.issue_action_schema:
        total[action] = 0

    for i in sql_users:  # TODO use this in get_users!
        sql_users[i]['password_hash'] = None
        users[sql_users[i]['id']] = sql_users[i]
        for new_field in shm.issue_action_schema:
            users[sql_users[i]['id']][new_field] = 0

    if sql_issues:
        for issue in sql_issues:
            users[issue['user_id']][issue['action']] += 1
            total[issue['action']] += 1

    total['first_name'] = '<b>Total</b>'
    total['last_name'] = ''
    users['total'] = total
    return users


def generate_dump_file(mysql, filetype):
    known_types = ('sql', 'xlsx', 'txz')
    if filetype == 'mysql':
        filetype = 'sql'
    if mysql is None or filetype not in known_types:
        return abort(404)

    filename = datetime.strftime(datetime.now(), 'Tracked_cases_'
                                 '%Y-%m-%d_%H-%m-%S_%f.' + filetype)
    if filetype == 'sql':
        dump_all2mysql(f_conf.MYSQL_USER, f_conf.MYSQL_PASSWORD,
                       f_conf.MYSQL_DB, filename)
    elif filetype == 'xlsx':
        dump_issues2xlsx(mysql, filename)
    elif filetype == 'txz':
        filename = '../flask_t1.tar.xz'

    if not os.path.isfile(filename):
        return abort(404)

    f = open(filename, 'r')
    response = make_response(f.read())
    f.close()
    if filetype != 'txz':
        os.remove(filename)

    response.headers['Content-Disposition'] = 'attachment; filename=' + filename
    return response


def generate_stat_chart(stat_dict):
    cd = {}  # cd for chart data
    for a in shm.issue_action_schema:
        cd[a] = ''
    cd['name'] = ''

    for u in stat_dict:
        if 'Total' not in stat_dict[u]['first_name'] \
                and 'Average' not in \
                stat_dict[u]['first_name']:
            for a in shm.issue_action_schema:
                cd[a] += str(int(stat_dict[u][a])) + ', '
            cd['name'] += '"' + stat_dict[u]['first_name'] + \
                          ' ' + stat_dict[u]['last_name'] + '", '

    return shm.script_chart % (cd['name'], cd['Created'], cd['Analyzed'],
                               cd['Tested'], cd['Implemented'], cd['Reviewed'],
                               cd['Moved'], cd['Updated'])

