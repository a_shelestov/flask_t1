-- PostgreSQL-formatted schemas:
---- issues schema
---- users schema

DROP TABLE IF EXISTS issues;
DROP TABLE IF EXISTS users;

CREATE TYPE action_enum_t AS ENUM (
    'Created', 'Analyzed', 'Tested', 'Implemented');

CREATE TYPE team_enum_t AS ENUM ('Navi', 'Speech');

CREATE TYPE ccb_priority_enum_t AS ENUM (
    'Any', 'Top', 'High', 'Medium', 'Low');

CREATE TYPE severity_enum_t AS ENUM (
    'Any', 'S-showstopper', 'A-Severe', 'B-Medium', 'C-Minor', 'D-Flow', 'NA');

CREATE TABLE issues (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT NOT NULL,
    action action_enum_t NOT NULL,
    action_date DATE
    user_id INTEGER NOT NULL,
    team team_enum_t NOT NULL,
    description TEXT NOT NULL,
    severity severity_enum_t NOT NULL,
    ccb_priority ccb_priority_enum_t NOT NULL
);

CREATE TABLE users{
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    password_hash TEXT NOT NULL,
    corporate_id TEXT NOT NULL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    team team_enum_t NOT NULL,
    is_admin BOOLEAN NOT NULL
}
