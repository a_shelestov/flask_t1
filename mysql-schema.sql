-- MySQL-formatted schemas:
-- - issues schema
-- - users schema

DROP TABLE IF EXISTS issues;
CREATE TABLE `issues` (
    `id` int(11) PRIMARY KEY AUTO_INCREMENT,
    `title` text NOT NULL,
    `action` enum('Created', 'Analyzed', 'Tested', 'Implemented', 'Reviewed', 'Moved', 'Updated') NOT NULL,
    `action_date` date,
    `user_id` int(11) NOT NULL,
    `description` text,
    `severity` enum('Any', 'S-showstopper', 'A-Severe', 'B-Medium', 'C-Minor', 'D-Flow', 'NA') NOT NULL,
    `ccb_priority` enum('Any', 'Top', 'High', 'Medium', 'Low') NOT NULL
);

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `password_hash` text NOT NULL,
  `corporate_id` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `team` enum('Navigation','Speech') NOT NULL,
  `is_admin` tinyint(1) NOT NULL
);

-- Sample data for testing
-- ! DO NOT USE THIS FOR PRODUCTION!

INSERT INTO `flask-acc`.`issues` (`id`, `title`, `action`, `action_date`, `user_id`, `description`, `severity`, `ccb_priority`)
VALUES
(NULL, 'Case#00006', 'Analyzed', '2015-09-22', 2, 'Some ridiculous nonsense from customer.', 'Any', 'Any'),
(NULL, 'Case#00013', 'Implemented', '2015-09-21', 1, '* now using /dev/null as persistence storage', 'Any', 'Any'),
(NULL, 'Case#00014', 'Created', '2015-10-31', 1, 'Weasels have eaten our phone cables.', 'S-showstopper', 'High'),
(NULL, 'Case#00015', 'Analyzed', '2015-11-11', 1, 'Coffee was spilled on production server.', 'Any', 'Any'),
(NULL, 'Case#00066', 'Tested', '2015-07-04', 2, 'Strange noises from server room.', 'Any', 'Any'),
(NULL, 'Case#00020', 'Analyzed', '2015-09-15', 1, 'LSD in water cooler', 'B-Medium', 'Medium'),
(NULL, 'Case#00088', 'Reviewed', '2015-11-02', 1, 'Somebody has killed black mouse.', 'C-Minor', 'Low');

INSERT INTO `flask-acc`.`users` (`id`, `password_hash`, `corporate_id`, `first_name`, `last_name`, `team`, `is_admin`)
VALUES
(NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'm019m1', 'John', 'Doe', 'Navigation', 1),
(NULL, '5f4dcc3b5aa765d61d8327deb882cf99', 'm019m2', 'Jane', 'Doe', 'Speech', 1);
