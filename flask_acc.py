#!/usr/bin/env python
__author__ = 'Andrew Shelestov'

import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from datetime import datetime

from flask import Flask, request, session, redirect, url_for, \
    abort, render_template, flash

from flask.ext.mysqldb import MySQL
import flask_acc_util as fau


app = Flask(__name__)
app.config.from_pyfile('flask_acc_config.py', silent=False)
mysql = MySQL()
mysql.init_app(app)


@app.route('/users')
def users():  # TODO: Remove this after debug is done!
    if fau.user_logged_in(session) and fau.user_is_admin(mysql, session['user_id']):
        cur = mysql.connection.cursor()
        cur.execute('''SELECT * FROM users;''')
        rv = cur.fetchall()
    else:
        rv = "None"
    return str(rv)


@app.route('/')
def page_main():
    if 'setting_show_range' not in session:
        session['setting_show_range'] = '20'

    if session['setting_show_range'] == 'all':
        limit = None
    else:
        limit = session['setting_show_range']
    return render_template('page_main.htm.j2',
                           issues=fau.get_issues(mysql, limit=limit),
                           users=fau.get_users(mysql))


@app.route('/add')
def page_add():
    return render_template('page_add.htm.j2')


@app.route('/issue/<int:issue_id>/<action>', methods=['GET', 'POST'])
def handle_issue(issue_id, action):
    __FUNCTION__ = sys._getframe().f_code.co_name + '()'

    if not fau.user_logged_in(session):
        abort(404)

    if issue_id is 0:  # Only adding allowed
        if action != 'add':
            fau.pr_err(__FUNCTION__, 'Unacceptable action for this issue!',
                       'page_main')
        else:
            fau.add_issue(mysql, session, request.form)
            if request.form['redirect'] == 'add':
                return redirect(url_for('page_add'))
            return redirect(url_for('page_main'))
    else:
        if action not in ['edit', 'delete']:
            fau.pr_err(__FUNCTION__, 'Unacceptable action for this issue!',
                       'page_main')
            return redirect(url_for('page_main'))

        found_issue = fau.get_issues(mysql, issue_id)
        if found_issue is None:
            fau.pr_err(__FUNCTION__, 'Issue #'+str(issue_id) + ' not found!',
                       'page_main')
            abort(404)

        if action == 'delete':
            fau.del_issue(mysql, session, found_issue)
            return redirect(url_for('page_main'))
        elif action == 'edit':
            if request.method == 'POST':
                retv = fau.edit_issue(mysql, session, request.form, found_issue)
                s_now = datetime.strftime(datetime.now(), '[%H:%m:%S]')
                flash(s_now + ' Editing ' + ('succeed' if retv else 'failed'))
                found_issue = fau.get_issues(mysql, issue_id)
            print("found_issue")
            print(found_issue)
            found_issue['title'] = found_issue['title'].replace('\"', "&quot;")
            return render_template('page_edit.htm.j2',
                                   issue_id=issue_id,
                                   issue_data=found_issue)
    abort(404)


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        cur = mysql.connection.cursor()
        cur.execute('SELECT id, corporate_id, password_hash, first_name, '
                    'last_name FROM users WHERE corporate_id="'
                    + request.form['username'] + '" LIMIT 1 ;')
        user = cur.fetchall()
        if user is ():  # empty query result, no user found
            error = 'Invalid username'
        else:
            user = user[0]
            if fau.password_hash(request.form['password']) == user[2]:
                session['logged_in'] = True
                session['user_id'] = user[0]
                session['user_login'] = request.form['username']
                session['user_name'] = user[3] + ' ' + user[4]
                flash('You were logged in as ' + session['user_login'] +
                      ' (' + session['user_login'] + ')')
                return redirect(url_for('page_main'))
            else:
                error = 'Invalid password'
    return render_template('page_login.htm.j2', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('page_main'))


@app.route('/user', methods=['GET', 'POST'])
def page_user():
    rqf = request.form
    if not fau.user_logged_in(session):
        abort(404)
    if request.method == 'POST':
        if rqf['new-password'] == rqf['new-password-conf']:
            retv = fau.password_update(mysql, session['user_id'],
                                       rqf['old-password'], rqf['new-password'])
            flash('Password update ' + ('succeed' if retv else 'failed'))
        else:
            flash('Error: passwords do not match!')
    return render_template('page_user.htm.j2')


@app.route('/xlsx')
def url_xlsx_dump():
    return fau.generate_dump_file(mysql, 'xlsx')


@app.route('/source')
def url_source_code():
    return fau.generate_dump_file(mysql, 'txz')


@app.route('/mysql')
def url_mysql_dump():
    return fau.generate_dump_file(mysql, 'sql')


@app.route('/statistics', methods=['GET', 'POST'])
def statistics():
    rqf = request.form
    dates = {'from': '', 'to': ''}

    if request.method == 'POST' and 'date_to' and 'date_from' in rqf and \
        fau.date_valid(rqf['date_to']) and fau.date_valid(rqf['date_from']):
        dates['from'] = rqf['date_from']
        dates['to'] = rqf['date_to']

    team_name = None
    ssr = 'setting_stat_range'
    if ssr in session and session[ssr] != 'All':
        team_name = session[ssr]
    # print(session)
    stat_dict_list = fau.calculate_stats(mysql, team_name, dates=dates)

    chart_str = fau.generate_stat_chart(stat_dict_list)

    return render_template('page_statistics.htm.j2',
                           stat_dict_list=stat_dict_list,
                           dates=dates,
                           stat_chart_data=chart_str)


@app.route('/settings/<key>/<value>/<redir>', methods=['GET', 'POST'])
def url_setting(key, value, redir):
    if fau.user_logged_in(session):
        session['setting_' + str(key)] = str(value)
        # print('key = ' + str(key) + '; value = ' + value, '; redirect = ' + redir)
        # print(session)
    return redirect(url_for(redir))


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.htm.j2'), 404


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
