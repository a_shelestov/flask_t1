# configuration
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'

# Now using MYSQL_* options, not MYSQL_DATABASE_*
MYSQL_USER = 'flask-acc'
MYSQL_PASSWORD = 'password'
MYSQL_DB = 'flask-acc'
MYSQL_HOST = '127.0.0.1'

