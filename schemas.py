user_schema = ('id', 'password_hash', 'corporate_id', 'first_name', 'last_name',
               'team', 'is_admin')

issue_action_schema = ('Created', 'Analyzed', 'Tested',
                       'Implemented', 'Reviewed', 'Moved', 'Updated')


def gen_user():  # Perverted dictionary constructor. Hope it simplifies my code.
    user = {}
    for field in user_schema:
        user[field] = None
    return user


script_chart = '\
    var barChartData = { \
        labels : [%s], \
        datasets : [ \
            {\
                fillColor : "rgba(120,144,120,0.5)", \
                strokeColor : "rgba(120,144,120,0.8)",\
                highlightFill: "rgba(120,144,120,0.75)",\
                highlightStroke: "rgba(120,144,120,1)",\
                data : [%s]\
            },\
            {\
                fillColor : "rgba(168,192,144,0.5)",\
                strokeColor : "rgba(168,192,144,0.8)",\
                highlightFill : "rgba(168,192,144,0.75)",\
                highlightStroke : "rgba(168,192,144,1)",\
                data : [%s]\
            },\
            {\
                fillColor : "rgba(216,216,144,0.5)",\
                strokeColor : "rgba(216,216,144,0.8)",\
                highlightFill : "rgba(216,216,144,0.75)",\
                highlightStroke : "rgba(216,216,144,1)",\
                data : [%s]\
            },\
            {\
                fillColor : "rgba(216,192,120,0.5)",\
                strokeColor : "rgba(216,192,120,0.8)",\
                highlightFill : "rgba(216,192,120,0.75)",\
                highlightStroke : "rgba(216,192,120,1)",\
                data : [%s]\
            },\
            {\
                fillColor : "rgba(168,144,96,0.5)",\
                strokeColor : "rgba(168,144,96,0.8)",\
                highlightFill : "rgba(168,144,96,0.75)",\
                highlightStroke : "rgba(168,144,96,1)",\
                data : [%s]\
            },\
            {\
                fillColor : "rgba(144,144,144,0.5)",\
                strokeColor : "rgba(144,144,144,0.8)",\
                highlightFill : "rgba(144,144,144,0.75)",\
                highlightStroke : "rgba(144,144,144,1)",\
                data : [%s]\
            },\
            {\
                fillColor : "rgba(200,200,200,0.5)",\
                strokeColor : "rgba(200,200,200,0.8)",\
                highlightFill : "rgba(200,200,200,0.75)",\
                highlightStroke : "rgba(200,200,200,1)",\
                data : [%s]\
            }\
        ]\
    }'
